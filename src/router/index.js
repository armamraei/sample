import Vue from 'vue'
import Router from 'vue-router'
 
import { routes } from './routes'

Vue.use(Router)

const router = new Router({
    linkActiveClass: 'is-active',
    mode: 'history',
    routes,
    base: process.env.NODE_ENV === 'production' ? '/cmp/' : ''
})


export default router
